سیستم حضور و غیاب اساتید
---

به کمک این سیستم بررسی فیزیکی حضور اساتید در دانشگاه برطرف میشود، میتوانند حضوری خود را در این سیستم اعلام کنند.


ویژگی های کلیدی سیستم حضور و غیاب اساتید
---
* مشخص نمودن فعالیت اساتید
* ثبت حضور اساتید به صورت سیستمی
* حذف بررسی فیزیکی حضوری اساتید

تحلیل و طراحی پروژه
---
* در ابتدا به بررسی چند عملکرد اصلی در نرم‌افزار در قالب سناریو عملکرد آزمون دهنده و سناریو عملکرد مدرس پرداخته ایم.  [سناریو ها](documentation/Scenario.md)
* [نیازمندی های پروژه](documentation/Requirments.md)
* [کلاس دیاگرام](documentation/ClassDiagram.md)
* [usecase](documentation/UseCaseDiagram.md)



فازهای توسعه پروژه
---
1. صفحه ورود کاربر
2. بخش ثبت حضور برای کاربر 1(مسئول ثبت)
3. بخش گزارش گیری برای کاربر 2(مدیر گروه)
4. بخش تخصیص/حذف ساعت کلاسی برای کاربر 2(مدیرگروه)
5. ...
6. ...


توسعه دهندگان
---

|نام و نام خانوادگی|   ID     |
|:----------------:|:--------:|
|     Majid Ghaderi     | @mghmajid |
|     Mohsen Hosseini     | @moh3en_h72 |
|     Tayebeh Zadsar     | @tayebehz |
