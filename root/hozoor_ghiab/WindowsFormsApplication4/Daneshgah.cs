﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication4
{
    class Daneshgah
    {
        public String name;
        public String addres;
        public String kind;
        public long number;

        public Daneshgah(String dname, String daddres, String dkind, long dnumber)
        {

            name = dname;
            addres = daddres;
            kind = dkind;
            number = dnumber;
        }
    }
}
