﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txt2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn1_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            string num = login.login_user(txt1.Text, txt2.Text);

            if (num == "wrong user")
            {
                MessageBox.Show("نام کاربری اشتباه است", "اخطار", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (num == "wrong pass")
            {
                MessageBox.Show("گذرواژه اشتباه است", "اخطار", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (num == "1")
            {
                M_amooozesh frm = new M_amooozesh(int.Parse(txt1.Text));
                frm.Show();
                this.Hide();
            }
            else if (num == "2")
            {
               
                D_manegment frm = new D_manegment(int.Parse(txt1.Text));
                frm.Show();
                this.Hide();
            }
            else if (num == "3")
            {
                M_sabt frm = new M_sabt(int.Parse(txt1.Text));
                frm.Show();
                this.Hide();
            }

            else
                MessageBox.Show("سیستم دچار مکشکل شده است", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btn2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
