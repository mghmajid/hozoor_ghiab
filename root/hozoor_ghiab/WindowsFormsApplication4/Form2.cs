﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class M_sabt : Form
    {
        Masool_sabt ms = new Masool_sabt();
        int kod_p;
        public M_sabt(int kod)
        {
            kod_p = kod;
            string a = ms.name(kod);
            InitializeComponent();
            lbl4.Text = a;
        }
        

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            
        }
        private void btn3_Click(object sender, EventArgs e)
        {
            try
            {
                ms.sabthozor(int.Parse(txt1.Text));
                MessageBox.Show("حضوری ثبت شد", "ثبت", MessageBoxButtons.OK);
                Refresh();
            }
            catch
            {
                MessageBox.Show("سیستم خراب است.بعدا تلاش کنید", "اخطار", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            lbl6.Visible = true;
           bool bl= ms.search(int.Parse(txt1.Text));
           if (bl == true)
           {
               lbl6.Text = "در این ساعت کلاس دارد";
               btn3.Enabled = true;
           }
           else
               lbl6.Text = "در این ساعت کلاس ندارد";
        }

        private void lbl4_Click(object sender, EventArgs e)
        {

        }

        private void lbl2_Click(object sender, EventArgs e)
        {

        }
    }
}
