﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication4
{
    class Person
    {
        public String name;
        public String last_name;
        public String madrak;
        public int personal_code;
        public long code_melli;
        public String samat;

        public Person(String pname, String plast_name, String pmadrak, long pcode_melli, int ppersonal_code, String psamat)
        {
            name = pname;
            last_name = plast_name;
            madrak = pmadrak;
            code_melli = pcode_melli;
            personal_code = ppersonal_code;

        }
        public Person()
        { }
    }
}
