﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace WindowsFormsApplication4
{
    class db_login
    {
        SQLiteConnection con = new SQLiteConnection("Data Source=hozoor_ghyab.sqlite;Version=3;");

        string q1 = "SELECT user,pass,type FROM Login WHERE user=?";

        public string[] login(string user)
        {
            string[] a = new string[3];
            SQLiteCommand cmd1 = new SQLiteCommand(q1, con);
            cmd1.Parameters.AddWithValue("@user", user);
            con.Open();
            SQLiteDataReader dr = cmd1.ExecuteReader();
            if (dr.Read())
            {
                a[0] = dr["user"].ToString();
                a[1] = dr["pass"].ToString();
                a[2] = dr["type"].ToString();
            }
            else
            {
                a[0] = "not";
            }
            con.Close();
            return a;
        }

    }
}
