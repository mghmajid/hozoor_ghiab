﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace WindowsFormsApplication4
{
    class db_sabt
    {

            SQLiteConnection con = new SQLiteConnection("Data Source=hozoor_ghyab.sqlite;Version=3;");

            string q1 = "SELECT name,family FROM Operator WHERE opId=?";
            string q2 = "SELECT profId,classId FROM ClassInfo WHERE profId=? AND day=? AND time=?";
            string q3 = "INSERT INTO Hoozor(classId,date) VALUES(?,?)";

            public string name(int user)
            {
                
                string a="a" ;
                SQLiteCommand cmd1 = new SQLiteCommand(q1, con);
                cmd1.Parameters.AddWithValue("@opId", user);
                con.Open();
                SQLiteDataReader dr = cmd1.ExecuteReader();
                if (dr.Read())
                {
                    a= dr["name"].ToString()+" "+dr["family"].ToString();
                }
                con.Close();
                return a;
            }
            public bool search_o(int kod)
            {
                DateTime now = DateTime.Now;
                string d = now.DayOfWeek.ToString();
                string h = now.Hour.ToString();
                string a="";

                SQLiteCommand cmd1 = new SQLiteCommand(q2, con);
                cmd1.Parameters.AddWithValue("@profId", kod);
                cmd1.Parameters.AddWithValue("@day", d);
                cmd1.Parameters.AddWithValue("@time", h);
                con.Open();
                SQLiteDataReader dr = cmd1.ExecuteReader();
                if (dr.Read())
                {
                    a = dr["profId"].ToString();
                }
                string k = kod.ToString();
                con.Close();
                if (k == a)
                    return true;
                else return false;

                
            }
            public void sabt_h(int kod)
            {
                DateTime now = DateTime.Now;
                string d = now.DayOfWeek.ToString();
                string h = now.Hour.ToString();
                string t = now.ToShortDateString();
                string class_id="";

                SQLiteCommand cmd1 = new SQLiteCommand(q2, con);
                cmd1.Parameters.AddWithValue("@profId", kod);
                cmd1.Parameters.AddWithValue("@day", d);
                cmd1.Parameters.AddWithValue("@time", h);
                con.Open();
                SQLiteDataReader dr = cmd1.ExecuteReader();
                if (dr.Read())
                   class_id = dr["classId"].ToString();
                con.Close();


                int cl = int.Parse(class_id);
                SQLiteCommand cmd2 = new SQLiteCommand(q3, con);
                cmd2.Parameters.AddWithValue("@classId", cl);
                cmd2.Parameters.AddWithValue("@date", t);
                con.Open();
                cmd2.ExecuteNonQuery();
                con.Close();


            }


        
    }
}
